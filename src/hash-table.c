#include "hash-table.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/** Get the length of string as an unsigned int **/
static unsigned int ustrlen(const char *string)
{
	unsigned int length = 0;

	assert(string != NULL);

	while (*(string++) != '\0')
		++length;

	return length;
}

/** FNV-1a Hash implementation **/
unsigned int hash(const char *key, const unsigned int elements)
{
	unsigned int hash=5381;
	unsigned int i=0;

	assert(key != NULL);

	for (i=0; i<ustrlen(key); ++i) {
		hash ^= key[i];
		hash *= 101;
	}

	return hash%elements;
}

struct hash_table_entry *hash_table_new_entry(char *key, void *data)
{
	struct hash_table_entry *entry;

	assert(key != NULL);

	entry = calloc(1, sizeof(struct hash_table_entry));

	entry->key = key;
	entry->data = data;
	entry->next = NULL;

	return entry;
}

struct hash_table *hash_table_init(const unsigned int init_capacity, const float rehash_threshold)
{
	struct hash_table *htable = 
		(struct hash_table *) calloc(1, sizeof(struct hash_table));

	assert(init_capacity > 0);

	/** Probably ran out of memory, recoverable/non fatal 
			error however (no assert needed) **/
	if (htable == NULL) {
		return NULL;
	}

	htable->bucket_count = init_capacity;
	htable->entry_count = 0;
	htable->rehash_threshold = rehash_threshold;
	htable->entries = 
		calloc(htable->bucket_count, sizeof(struct hash_table_entry *));

	return htable;
}

struct hash_table_entry *hash_table_insert(struct hash_table *htable, 
	struct hash_table_entry *new_entry)
{
	struct hash_table_entry **ap = NULL;
	unsigned int hash_value = 0;
	int key_exists = 0; /** Does an entry using the 'new_entry' 
					key already exist? **/

	assert(htable != NULL);
	assert(new_entry != NULL);
	assert(htable->bucket_count >= 0);
	assert(htable->entry_count <= htable->bucket_count);

	hash_value = hash(new_entry->key, htable->bucket_count);

	/** Find the last entry in the current bucket (each bucket uses
	    seperate chaining as its strategy for managing collisions) **/
	ap = &htable->entries[hash_value];
	while ((*ap) != NULL ) {
		if (strncmp(new_entry->key, (*ap)->key, strlen(new_entry->key)) == 0) {
			return *ap;
		}

		ap = &((*ap)->next);
	}
	
	++(htable->entry_count);
	*ap = new_entry;

	return NULL;
}

struct hash_table_entry *hash_table_get(struct hash_table *htable, char *key)
{
	struct hash_table_entry *ap = NULL;

	assert(htable != NULL);

	ap = htable->entries[hash(key, htable->bucket_count)];

	/** Search the side chained entries too **/
	while (ap != NULL ) {
		if (strncmp(key, (ap)->key, ustrlen(key)) == 0) {
			return ap;
		}

		ap = ap->next;
	}

	return NULL;
}

void hash_table_destroy(struct hash_table *htable)
{
	int i=0;

	if (htable != NULL) {
		for (i=0; i<htable->bucket_count; ++i) {
			free(htable->entries[i]); 
		}
		free(htable->entries);
		free(htable);
	}
}

void hash_table_dump_all(struct hash_table *htable, FILE *stream)
{
	struct hash_table_entry *ap;
	int i=0;

	for (i=0; i<htable->bucket_count; ++i) {
		if (htable->entries[i] == NULL) continue;

		ap = htable->entries[i];
		while (ap != NULL) {
			fprintf(stream, "%s -> %s \n", ap->key, (char *)ap->data);
			ap = ap->next;
		}

	}
}

void hash_table_dump_keys(struct hash_table *htable, FILE *stream)
{
	struct hash_table_entry *ap;
	int i=0;

	for (i=0; i<htable->bucket_count; ++i) {
		if (htable->entries[i] == NULL) continue;

		ap = htable->entries[i];
		while (ap != NULL) {
			fprintf(stream, "%s \n", ap->key);
			ap = ap->next;
		}

	}
}

void hash_table_dump_values(struct hash_table *htable, FILE *stream)
{
	struct hash_table_entry *ap;
	int i=0;

	for (i=0; i<htable->bucket_count; ++i) {
		if (htable->entries[i] == NULL) continue;

		ap = htable->entries[i];
		while (ap != NULL) {
			fprintf(stream, "%s \n", (char *)ap->data);
			ap = ap->next;
		}

	}
}
