#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "hash-table.h"

#define LOAD_RATIO 4 /** Number of available spaces per entry, 4 = 3/4 load ratio **/
#define ACCEPTABLE_HASH_COLLISION_RATE 20 /** As a percentage of the overall entry count **/
#define ENTRY_COUNT 42 /** Number of elements in test_data **/
static char *test_data[ENTRY_COUNT] = /** Test data **/
		{"London", "Bath", "Bristol", "Exeter", "Gloucester", 
		"Plymouth", "Swindon", "Bournemouth", "Brighton", "Guildford",
		"Milton Keynes", "Oxford", "Portsmouth", "Reading", "Southampton",
		"Cambridge", "Ipswich", "Luton", "Norwich", "Aberdeen", "Edinburgh", 
		"Glasgow", "Inverness", "Middlesbrough", "Newcastle", "Sunderland",
		"Liverpool",  "Manchester", "Derby", "Leicester", "Nottingham",
		"Birmingham", "Coventry", "Rugby", "Stoke-on-Trent", "Bradford",
		"Hull", "Leeds", "Sheffield", "York", "Cardiff", "Swansea"};
static char *test_nonsense_data[ENTRY_COUNT] = /** Test data **/
		{"sfdsggtt", "dfgDSFGfg", "werger", "dgdfgdrr", "rgdrgdrrr", 
		"rtrtg", "kkt sksdfjf wfiw", "b  cbcvb", "$%%£$43r3", "drgdrrr",
		"rthrt5456****4", "45", "fg", "dgee44", "dg634545hh",
		"hdgkjkrt", " thth eh", "ergerjklsrf", "drg", "4eg", "er", 
		"sss", "£$%%sfsdf£$%%", "6uiw47yr", "t", "eryhjetj dtg",
		"gergerg",  "erger", "ergerg", "erger", "drgrgd",
		"ergreg", "khju77", " cghcgymgjn", "rjs cfmr", "rfn cryfn gyr ",
		"w", "4w", "fg", "64y)(*&", "yyyyfn ", "f mytm h"};
static unsigned int value_upper_range = ENTRY_COUNT*LOAD_RATIO;
static float rehash_value = 1.0/LOAD_RATIO;

static int test_hash_function()
{
	int collisions=0;
	int i=0;
	int j=0;
	float fail_rate = 0.0;
	int entrys[ENTRY_COUNT];

	for (i=0; i<ENTRY_COUNT; ++i) {
		entrys[i] = 0;
	}

	for (i=0; i<ENTRY_COUNT; ++i) {
		entrys[i] = hash(test_data[i], value_upper_range);
		for (j=0; j<i; ++j) {
			if (entrys[i] == entrys[j]) {
				++collisions;
				break;
			}
		}
	}

	printf("[test_hash_function] %d collisions detected with %d spaces for every pair.\n", 
		collisions, value_upper_range/ENTRY_COUNT);
	fail_rate = ((float)collisions/(float)ENTRY_COUNT)*100;
	printf("[test_hash_function] %2.1f%% entry failure rate.\n", fail_rate);

	if (fail_rate > ACCEPTABLE_HASH_COLLISION_RATE) {
		printf("[test_hash_function] Collision rate is too high, something is probably wrong. \n");
		return 0;
	}

	return 1;
}

static int test_hash_table_init()
{
	unsigned int i=0;
	struct hash_table *htable = hash_table_init(value_upper_range, rehash_value);

	if (htable == NULL) {
		printf("[test_hash_table_init] Null pointer returned. \n");
		return 0;
	}

	if (htable->rehash_threshold != rehash_value) {
		printf("[test_hash_table_init] Incorrect rehash_threshold value. \n");
		return 0;
	}

	if (htable->bucket_count != value_upper_range) {
		printf("[test_hash_table_init] Incorrect entry_capacity value. \n");
		return 0;
	}

	for (i=0; i<htable->bucket_count; ++i) {
		if (htable->entries[i] != NULL) {
			printf("[test_hash_table_init] Uninitialized entry in entries. \n");
			return 0;
		}
	}

	return 1;
}

static int test_hash_table_insert()
{
	struct hash_table_entry *entry = NULL;
	struct hash_table *htable = NULL;
	int i=0;
	unsigned int key_hash=0;
	int found = 0;
	struct hash_table_entry *ap = NULL;

	htable = hash_table_init(value_upper_range, rehash_value);
	for (i=0; i<ENTRY_COUNT; ++i) {
		entry = hash_table_new_entry(test_data[i], test_data[i]);
		key_hash = hash(test_data[i], value_upper_range);

		if (hash_table_insert(htable, entry) != NULL) {
			printf("[test_hash_table_insert] insert(...) error code returned from insert call. \n");
			return 0;
		}

		/** Try and locate the entry, check chained entries too **/
		ap = htable->entries[key_hash];
		while (ap != NULL && found == 0) {
			if (ap == entry) break;
			ap = ap->next;
		}

		if (ap == NULL) {
			printf("[test_hash_table_insert] insert(...) entry not found after insertion. \n");
			return 0;
		}

		if (strncmp(ap->data, test_data[i], strlen(test_data[i])) != 0) {
			printf("[test_hash_table_insert] insert(...) entry data is incorrect. \n");
			return 0;
		}

		if (strncmp(ap->key, test_data[i], strlen(test_data[i])) != 0) {
			printf("[test_hash_table_insert] insert(...) entry key is incorrect. \n");
			return 0;
		}
	}

	if (ENTRY_COUNT != htable->entry_count) {
		printf("[test_hash_table_insert] insert(...) entry count is incorrect. \n");
		return 0;
	}
	
	/** Try to reinsert the same keys, it should reject them **/
	for (i=0; i<ENTRY_COUNT; ++i) {
		entry = hash_table_new_entry(test_data[i], test_data[i]);
		key_hash = hash(test_data[i], value_upper_range);

		if (hash_table_insert(htable, entry) == NULL) {
			printf("[test_hash_table_insert] insert(...) allowed a duplicate key entry. \n");
			return 0;
		}
	}

	if (ENTRY_COUNT != htable->entry_count) {
		printf("[test_hash_table_insert] insert(...) entry count is incorrect. \n");
		return 0;
	}

	return 1;
}

static int test_hash_table_get()
{
	struct hash_table_entry *entry = NULL;
	struct hash_table *htable = NULL;
	int i=0;
	unsigned int key_hash=0;

	/** Build and populate the hash table **/
	htable = hash_table_init(value_upper_range, rehash_value);
	for (i=0; i<ENTRY_COUNT; ++i) {
		entry = hash_table_new_entry(test_data[i], test_data[i]);

		key_hash = hash(test_data[i], value_upper_range);

		if (hash_table_insert(htable, entry) != NULL) {
			printf("[test_hash_table_get] insert(...) error code returned from insert call. \n");
			return 0;
		}
	}
	
	if (ENTRY_COUNT != htable->entry_count) {
		printf("[test_hash_table_get] insert(...) entry count is incorrect. \n");
		return 0;
	}

	/** Attempt to retrieve valid values **/
	for (i=0; i<ENTRY_COUNT; ++i) {
		if (hash_table_get(htable, test_data[i]) == NULL) {
			printf("[test_hash_table_get] get(...) could not retrieve key-value pair. \n");
			return 0;
		}
	}

	/** Attempt to retrieve nonsense **/
	for (i=0; i<ENTRY_COUNT; ++i) {
		if (hash_table_get(htable, test_nonsense_data[i]) != NULL) {
			printf("[test_hash_table_get] get(...) retrieved an incorrect key-value pair. \n");
			return 0;
		}
	}

	return 1;
}

int main()
{
	if (!test_hash_function()) {
		printf("[main] (test-hash-table) hash(...) (!) FAIL (see previous messages). \n");
		return -1;
	} else {
		printf("[main] (test-hash-table) hash(...) PASS. \n");
	}

	if (!test_hash_table_init()) {
		printf("[main] (test-hash-table) init(...) (!) FAIL (see previous messages). \n");
		return -1;
	} else {
		printf("[main] (test-hash-table) init(...) PASS. \n");
	}

	if (!test_hash_table_insert()) {
		printf("[main] (test-hash-table) insert(...) (!) FAIL (see previous messages). \n");
		return -1;
	} else {
		printf("[main] (test-hash-table) insert(...) PASS. \n");
	}

	if (!test_hash_table_get()) {
		printf("[main] (test-hash-table) get(...) (!) FAIL (see previous messages). \n");
		return -1;
	} else {
		printf("[main] (test-hash-table) get(...) PASS. \n");
	}

	return 0;
}
