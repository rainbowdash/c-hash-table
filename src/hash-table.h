#ifndef HASH_TABLE_H
#define HASH_TABLE_H

#include <stdio.h>

/** Chained table entry, can represent multiple key/value pairs (seperate chaining) **/
struct hash_table_entry {
	struct hash_table_entry *next;
	char *key; /** Unhashed key **/
	void *data;
};

struct hash_table {
	unsigned int bucket_count;
	unsigned int entry_count;
	float rehash_threshold;
	struct hash_table_entry **entries;
};

extern unsigned int hash(const char *key, const unsigned int elements);

extern struct hash_table *hash_table_init(const unsigned int init_capacity, 
	const float rehash_threshold);

extern struct hash_table_entry *hash_table_new_entry(char *key, void *data);

/** If the key is in use, return the entry using the key. Return NULL on success. */
extern struct hash_table_entry *hash_table_insert(struct hash_table *htable, 
	struct hash_table_entry *new_entry);

extern struct hash_table_entry *hash_table_get(struct hash_table *htable, char *key);

extern void hash_table_destroy(struct hash_table *htable);

extern void hash_table_dump_all(struct hash_table *htable, FILE *stream);

extern void hash_table_dump_keys(struct hash_table *htable, FILE *stream);

extern void hash_table_dump_values(struct hash_table *htable, FILE *stream);

/** TODO: Rehash function **/

#endif
